/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.helper;

import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.stash.server.ApplicationPropertiesService;
import de.aeffle.stash.plugin.hook.httpLocation.AbstractHttpLocation;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationUntranslated;
import org.apache.commons.lang.text.StrSubstitutor;

import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

public class UrlTemplateTranslator {
	private Map<String, String> translationMap;

	public UrlTemplateTranslator(
		    ApplicationPropertiesService applicationPropertiesService,
			StashAuthenticationContext stashAuthenticationContext,
			RepositoryHookContext repositoryHookContext,
			RefChange refChange) {
		translationMap = new HashMap<String, String>();

        if (applicationPropertiesService != null) {
            addApplicationPropertiesContext(applicationPropertiesService);
        }

		if (stashAuthenticationContext != null) { 
			addStashAuthenticationContext(stashAuthenticationContext);
		}
		
		if (repositoryHookContext != null) {
			addRepositoryHookContext(repositoryHookContext);
		}
		
		if (refChange != null) {
			addRefChange(refChange);
		}
	}

    private void addApplicationPropertiesContext(ApplicationPropertiesService applicationPropertiesService) {
        URI baseUrl = applicationPropertiesService.getBaseUrl();

        addTranslation("baseUrl", baseUrl.toString());
        addTranslation("baseUrl.protocol", baseUrl.getScheme());
        addTranslation("baseUrl.host", baseUrl.getHost());
        addTranslation("baseUrl.port", ((Integer) baseUrl.getPort()).toString());
        addTranslation("baseUrl.path", baseUrl.getPath());
    }

    public void addRepositoryHookContext(RepositoryHookContext repositoryHookContext) {
		Repository repository = repositoryHookContext.getRepository();
		addTranslation("repository.id", repository.getId().toString());
		addTranslation("repository.name", repository.getName());
		addTranslation("repository.slug", repository.getSlug());
		addTranslation("project.name", repository.getProject().getName());
		addTranslation("project.key", repository.getProject().getKey());
        addTranslation("project.key.lower", repository.getProject().getKey().toLowerCase());
    }

	public void addStashAuthenticationContext(StashAuthenticationContext stashAuthenticationContext) {
		StashUser user = stashAuthenticationContext.getCurrentUser();
		addTranslation("user.name", user.getName());
		addTranslation("user.displayName", user.getDisplayName());
		addTranslation("user.email", user.getEmailAddress());
	}
	
	public void addRefChange(RefChange refChange) {
	    addTranslation("refChange.refId",  refChange.getRefId());
		addTranslation("refChange.fromHash",  refChange.getFromHash());
		addTranslation("refChange.toHash",  refChange.getToHash());
		addTranslation("refChange.type",  refChange.getType().toString());
		
		addTranslation("refChange.name",  refChange.getRefId().replaceAll("^refs/(tags|heads)/", ""));
	}
	
	private void addTranslation(String key, String value) {
		translationMap.put(key, value);
	}

	public HttpLocationTranslated translate(HttpLocationUntranslated httpLocationUntranslated) {
        //TODO: Clone und cast does not work :-/
        // HttpLocationTranslated httpLocationTranslated = (HttpLocationTranslated) httpLocationUntranslated.clone();
        HttpLocationTranslated httpLocationTranslated = HttpLocationTranslated.newInstance(httpLocationUntranslated);

        StrSubstitutor strSubstitutor = new StrSubstitutor(translationMap);
		String urlTemplate = httpLocationUntranslated.getUrlTemplate();
		String url = strSubstitutor.replace(urlTemplate);

		httpLocationTranslated.setUrl(url);

        return httpLocationTranslated;
	}

}
