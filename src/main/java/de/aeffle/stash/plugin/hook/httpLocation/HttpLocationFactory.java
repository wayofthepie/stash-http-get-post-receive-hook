/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.httpLocation;

import com.atlassian.stash.hook.repository.RepositoryHookContext;

import java.util.ArrayList;

public class HttpLocationFactory {
	private static RepositoryHookContext context;

    public HttpLocationFactory(RepositoryHookContext context) {
        this.context = context;
    }

    public ArrayList<HttpLocationUntranslated> getAllFromContext() {
		ArrayList<HttpLocationUntranslated> httpGetLocations = new ArrayList<HttpLocationUntranslated>();

		for (int id = 1; id <= getNumberOfHttpLocations(context); id++) {
			httpGetLocations.add(getUntranslatedHttpLocation(id));
		}

		return httpGetLocations;
	}

	private HttpLocationUntranslated getUntranslatedHttpLocation(int id) {
		String urlString = ( id > 1 ? "url" + id : "url" );
	    String httpMethodString = ( id > 1 ? "httpMethod" + id : "httpMethod" );
        String postDataString = ( id > 1 ? "postData" + id : "postdata" );

		String useAuthString = ( id > 1 ? "use_auth" + id : "use_auth" );
		if (getVersionNumber() > 1) {
			useAuthString = ( id > 1 ? "useAuth" + id : "useAuth" );
		}
		
		String userString = ( id > 1 ? "user" + id : "user" );
		String passString = ( id > 1 ? "pass" + id : "pass" );

        String branchFilterString = ( id > 1 ? "branchFilter" + id : "branchFilter" );
        String tagFilterString = ( id > 1 ? "tagFilter" + id : "tagFilter" );
        String userFilterString = ( id > 1 ? "userFilter" + id : "userFilter" );

        HttpLocationUntranslated httpLocationUntranslated = new HttpLocationUntranslated();

		httpLocationUntranslated.setUrlTemplate(getConfigString(urlString));
        httpLocationUntranslated.setHttpMethod(getConfigString(httpMethodString));
        httpLocationUntranslated.setPostData(getConfigString(postDataString));

        httpLocationUntranslated.setUseAuth(getConfigBoolean(useAuthString));
        httpLocationUntranslated.setUser(getConfigString(userString));
        httpLocationUntranslated.setPass(getConfigString(passString));

        httpLocationUntranslated.setBranchFilter(getConfigString(branchFilterString));
        httpLocationUntranslated.setTagFilter(getConfigString(tagFilterString));
        httpLocationUntranslated.setUserFilter(getConfigString(userFilterString));

        return httpLocationUntranslated;
	}

	private int getVersionNumber() {
		int version;
		try {
			String versionString = context.getSettings().getString("version", "1");
			version = Integer.parseInt(versionString); 
		} catch (Exception e) {
			version = 1;
		}
		return version;
	}

	private String getConfigString(String name) {
		return context.getSettings().getString(name, "");
	}

	private boolean getConfigBoolean(String name) {
		return context.getSettings().getBoolean(name, false);
	}
	
	private static int getNumberOfHttpLocations(RepositoryHookContext context) {
		 int count;
		 try {
			 count = Integer.parseInt(context.getSettings().getString("locationCount", "1"));
		 } catch (Exception e) {
			 count = 1;
		 }
		 
		 return (count > 0 ? count : 1);
    }

}