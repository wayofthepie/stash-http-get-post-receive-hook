/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook;

import java.util.Collection;
import java.util.List;

import com.atlassian.extras.common.log.Logger;
import com.atlassian.stash.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.atlassian.stash.user.StashAuthenticationContext;
import de.aeffle.stash.plugin.hook.helper.ContextFilter;
import de.aeffle.stash.plugin.hook.helper.HttpAgent;
import de.aeffle.stash.plugin.hook.helper.UrlTemplateTranslator;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationFactory;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationUntranslated;
import de.aeffle.stash.plugin.hook.validators.FilterValidator;
import de.aeffle.stash.plugin.hook.validators.LocationCountValidator;
import de.aeffle.stash.plugin.hook.validators.ValidationError;
import de.aeffle.stash.plugin.hook.validators.UrlValidator;

public class HttpGetPostReceiveHook implements AsyncPostReceiveRepositoryHook,
		RepositorySettingsValidator {

	private static final Logger.Log log = Logger
			.getInstance(HttpGetPostReceiveHook.class);
    private final ApplicationPropertiesService applicationPropertiesService;
	private final StashAuthenticationContext stashAuthenticationContext;

	public HttpGetPostReceiveHook(
            ApplicationPropertiesService applicationPropertiesService,
			StashAuthenticationContext stashAuthenticationContext
            ) {
        this.applicationPropertiesService = applicationPropertiesService;
		this.stashAuthenticationContext = stashAuthenticationContext;
	}

	/**
	 * Connects to a configured URL to notify of all changes.
	 */
	@Override
	public void postReceive(RepositoryHookContext repositoryHookContext,
			Collection<RefChange> refChanges) {
        HttpLocationFactory httpLocationFactory = new HttpLocationFactory(repositoryHookContext);
		Collection<HttpLocationUntranslated> httpLocations = httpLocationFactory.getAllFromContext();

		log.debug("Http Get Post Receive Hook started.");
        log.debug("User: " + this.stashAuthenticationContext.getCurrentUser().getName());
		log.debug("Number of HttpLocations: " + httpLocations.size());
		
		for (RefChange refChange : refChanges) {
		    log.debug("RefChange: " + refChange.getRefId());

            for (HttpLocationUntranslated httpLocationUntranslated : httpLocations) {
		        log.debug("HttpLocationUntranslated: " + httpLocationUntranslated);
		        
		        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                                                                             stashAuthenticationContext,
                                                                             repositoryHookContext,
                                                                             refChange);
		        HttpLocationTranslated httpLocationTranslated = translator.translate(httpLocationUntranslated);
                log.debug("HttpLocationTranslated: " + httpLocationTranslated);

                ContextFilter contextFilter = new ContextFilter(stashAuthenticationContext, refChange);

                if (contextFilter.checkIfFilterMatches(httpLocationTranslated)) {
                    HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
                    httpAgent.doPageRequest();
                }
		    }
		}
	}

	@Override
	public void validate(Settings settings, SettingsValidationErrors errors,
			Repository repository) {

        LocationCountValidator locationCountValidator = new LocationCountValidator(settings);

        if (locationCountValidator.isValid() == false) {
            List<ValidationError> locationCountValidatorErrors = locationCountValidator.getErrors();

            for (ValidationError error : locationCountValidatorErrors) {
                errors.addFieldError(error.getKey(), error.getValue());
            }
            return;
        }


        UrlValidator urlValidator = new UrlValidator(settings);

        if (urlValidator.isValid() == false) {
            List<ValidationError> urlValidationErrors = urlValidator.getErrors();

            for (ValidationError error : urlValidationErrors) {
                errors.addFieldError(error.getKey(), error.getValue());
            }
            return;
        }

        FilterValidator filterValidator = new FilterValidator(settings);

        if (filterValidator.isValid() == false) {
            List<ValidationError> filterErrors = filterValidator.getErrors();

            for (ValidationError error : filterErrors) {
                errors.addFieldError(error.getKey(), error.getValue());
            }
            return;
        }
	}




}