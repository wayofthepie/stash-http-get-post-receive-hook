/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.helper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.aeffle.stash.plugin.hook.HttpGetPostReceiveHook;
import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;
import org.apache.commons.codec.binary.Base64;

import com.atlassian.extras.common.log.Logger;

public class HttpAgent {
	private static final Logger.Log log = Logger.getInstance(HttpGetPostReceiveHook.class);
    private final String USER_AGENT = "Mozilla/5.0";

    private final String urlString;
    private final String httpMethod;
    private final String postData;

	private final String user;
	private final String pass;
	private final Boolean useAuth;

    private HttpURLConnection httpURLConnection;

	public HttpAgent(HttpLocationTranslated httpLocation) {
		urlString = httpLocation.getUrl();
        httpMethod = httpLocation.getHttpMethod();
		postData = httpLocation.getPostData();

        useAuth = httpLocation.getUseAuth();
        user = httpLocation.getUser();
	    pass = httpLocation.getPass();
	    
		log.info("Http request with URL: " + urlString + " (" + httpMethod + ")");

	}


	public void doPageRequest() {
		try {
			if (httpURLConnection == null) {
				URL url = new URL(urlString);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                log.info("doPageRequest: " + url.getHost());
            }
			
			httpURLConnection.setReadTimeout(5000);
            httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);

            if (useAuth == true) {
				authenticate(user, pass);
			}

            if (httpMethod.equals("POST") || httpMethod.equals("PUT")) {
                log.info("Start with the POST or PUT");
                doPost();
            } else {
                if (httpMethod.equals("GET") || httpMethod.equals("")) {
                    log.info("Start with the GET: " );
                    doGet();
                }
            }
		}
        catch (MalformedURLException e) {
            log.error("Malformed URL:" + e);
        }
        catch (IOException e) {
            log.error("Some IO exception occured " + e.getMessage() + " (" + e.getStackTrace() + ")");
        }
        catch (Exception e) {
            log.error("Something else went wrong: ", e);
        }
	}


    private void authenticate(String user, String pass) {
        log.info("Authentication was enabled with user: " + user);

        // build the auth string
        String authString = user + ":" + pass;
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));

        httpURLConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
    }


    private void doGet() throws IOException {
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setFollowRedirects(true);
        httpURLConnection.connect();

        checkResponseCode();
        logResponseBody();

        httpURLConnection.disconnect();
    }

    private void doPost() throws IOException {
        httpURLConnection.setRequestMethod(httpMethod);
        httpURLConnection.setRequestProperty("charset", "utf-8");
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Content-Length", "" + Integer.toString(postData.getBytes().length));
        httpURLConnection.setRequestProperty("Content-Language", "en-US");

        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setUseCaches (false);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);

        // Send post request
        DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream ());
        wr.writeBytes(postData);
        wr.flush();
        wr.close();

        checkResponseCode();
        logResponseBody();

        httpURLConnection.disconnect();
    }

    private void checkResponseCode() throws IOException {
        // Get HTTP Response Code
        int responseCode = httpURLConnection.getResponseCode();

        if (responseCode < 200 || responseCode >= 300) {
            log.error("Problem with the HTTP connection with response code: "
                    + responseCode);
        }
    }

    private void logResponseBody() throws IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                httpURLConnection.getInputStream()));
        String inputLine;
        StringBuffer body = new StringBuffer();

        while ((inputLine = buffer.readLine()) != null) {
            body.append(inputLine + "\n");
        }
        // close everything
        buffer.close();

        String content = body.toString();
        log.debug("HTTP response:\n" + content);
    }

    // For testing purposes to inject a connection:
    public void setConnection(HttpURLConnection httpURLConnection) {
        this.httpURLConnection = httpURLConnection;
    }

}
