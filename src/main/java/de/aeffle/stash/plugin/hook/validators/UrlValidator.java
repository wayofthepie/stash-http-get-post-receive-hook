/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.validators;


import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UrlValidator extends Validator {
    public UrlValidator(Settings settings) {
        super(settings);
    }

    @Override
    public boolean isValid() {
        int locationCount = getLocationCount();
        boolean result = true;

        for (int i = 1; i <= locationCount; i++) {
            boolean isValidUrl = validateUrl(i);
            if (!isValidUrl) {
                result = false;
            }
        }

        return result;
    }

    private boolean validateUrl(int id) {
        String urlName = (id > 1 ? "url" + id : "url");
        String urlString = settings.getString(urlName, "");

        if (urlString.isEmpty()) {
            addError(urlName,
                    "Url field is blank, please supply one.");
            return false;
        } else {
            try {
                URL url = new URL(urlString);
                String protocol = url.getProtocol();
                List<String> validProtocols = new ArrayList<String>();
                validProtocols.add("http");
                validProtocols.add("https");

                if (!validProtocols.contains(protocol)) {
                    addError(urlName,
                            "Url did not contain a valid http(s) URL.");
                    return false;
                }
            } catch (MalformedURLException e) {
                addError(urlName, "Url was malformed.");
                return false;
            }
            return true;
        }
    }
}
