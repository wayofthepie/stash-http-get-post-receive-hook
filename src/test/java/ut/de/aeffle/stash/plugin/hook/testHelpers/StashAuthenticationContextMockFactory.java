/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.testHelpers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;


public class StashAuthenticationContextMockFactory {

	private StashAuthenticationContext stashAuthenticationContext;
	private StashUser stashUser;

	public StashAuthenticationContextMockFactory() {
		clear();
	}

	public StashAuthenticationContextMockFactory clear() {
		createNewMocks();
		loadDefaults();
		return this;
	}

	private void createNewMocks() {
		stashAuthenticationContext = mock(StashAuthenticationContext.class);
		stashUser = mock(StashUser.class);
		
		when(stashAuthenticationContext.getCurrentUser()).thenReturn(stashUser);
	}

	private void loadDefaults() {
		when(stashUser.getDisplayName()).thenReturn("");
		when(stashUser.getEmailAddress()).thenReturn("");
		when(stashUser.getName()).thenReturn("");
        when(stashUser.getSlug()).thenReturn("");
	}

	public void setDisplayName(String name) {
		when(stashUser.getDisplayName()).thenReturn(name);
	}
	
	public void setEmailAddress(String email) {
		when(stashUser.getEmailAddress()).thenReturn(email);
	}
	
	public void setName(String name) {
		when(stashUser.getName()).thenReturn(name);
	}

    public void setSlug(String slug) {
        when(stashUser.getSlug()).thenReturn(slug);
    }

	public StashAuthenticationContext getContext() {
		return stashAuthenticationContext;
	}
	
}
