/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.helper;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.net.HttpURLConnection;

import de.aeffle.stash.plugin.hook.httpLocation.HttpLocationTranslated;
import org.junit.Test;

import de.aeffle.stash.plugin.hook.helper.HttpAgent;

public class HttpAgentTest {

	@Test
	public void testDoPageRequestWithoutAuth() {
        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();

        httpLocationTranslated.setUseAuth(false);

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);

		try {
			HttpURLConnection connection = mock(HttpURLConnection.class);
			when(connection.getResponseCode()).thenReturn(HttpURLConnection.HTTP_OK);
			
			httpAgent.setConnection(connection);
			httpAgent.doPageRequest();

			verify(connection, times(1)).connect();
			verify(connection, never()).setRequestProperty(eq("Authorization"), anyString());
			
		} catch (IOException e) {
			fail("IOException");
		}
	}
	
	@Test
	public void testDoPageRequestWithAuth() {
        HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();

        httpLocationTranslated.setUseAuth(true);
        httpLocationTranslated.setUser("john.doe");
        httpLocationTranslated.setPass("secret");

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);

		try {
			HttpURLConnection connection = mock(HttpURLConnection.class);
			when(connection.getResponseCode()).thenReturn(HttpURLConnection.HTTP_OK);
			
			httpAgent.setConnection(connection);
			httpAgent.doPageRequest();

			verify(connection, times(1)).connect();
			verify(connection, times(1)).setRequestProperty("Authorization", "Basic am9obi5kb2U6c2VjcmV0");
			
		} catch (IOException e) {
			fail("IOException");
		}
	}

	@Test
	public void testDoBadPageRequestWithAuth() {
		HttpLocationTranslated httpLocationTranslated = new HttpLocationTranslated();

        httpLocationTranslated.setUseAuth(true);
        httpLocationTranslated.setUser("john.doe");
        httpLocationTranslated.setPass("secret");

		HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);

		try {
			HttpURLConnection connection = mock(HttpURLConnection.class);
			when(connection.getResponseCode()).thenReturn(HttpURLConnection.HTTP_BAD_REQUEST);
			
			httpAgent.setConnection(connection);
			httpAgent.doPageRequest();

			verify(connection, times(1)).connect();
			verify(connection, times(1)).setRequestProperty("Authorization", "Basic am9obi5kb2U6c2VjcmV0");
			
		} catch (IOException e) {
			fail("IOException");
		}
	}


}
